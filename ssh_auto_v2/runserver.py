#!/usr/bin/env python3
''' ssh 权限自动化开通系统'''

__author__ = 'cali'
from app.app import APP
from conf import config
from conf.config import *
from flask import render_template
import time
from flask import redirect
from flask import request
from flask_restful import Resource

app = APP(config)
db = app.get_db()
api = app.get_api()

print('hello,world')
class user(db.Model):
    __tablename__ = "user"
    user_id = db.Column(db.Integer,primary_key=True)
    username = db.Column(db.String)
    password = db.Column(db.String)
    department = db.Column(db.String)
    manager = db.Column(db.String)
    date = db.Column(db.Date)
class  userApi(Resource):
    def post(self):
        db.session.begin()
        user_info = user(username=request.form['username'],password=request.form['password'],department=request.form['department'],manager=request.form['manager'],date=time.strftime('%Y%m%d'))
        print(user_info)
        db.session.add(user_info)
        db.session.commit()
        db.session.close()
        return "register ok 注册成功",200
    def get(self):
        pass
    def delete(self):
        pass
    def put(self):
        pass
    def options(self):
        pass
api.add_resource(userApi,'/v1/api/user')

@app.route('/register_user_db',methods=["POST"])
def add_user_info():
    try:
        user_info = user(username=request.form['username'], password=request.form['password'], sex=request.form['sex'],department=request.form['department'], manager=request.form['manager'],date=time.strftime('%Y%m%d'))
        db.session.add(user_info)
        db.session.commit()
        return "注册成功"
    except:
        return "用户名已经存在，请重新输入"

#首页
@app.route('/')
def x_index():
    return render_template('index.html')
@app.route('/denglu')
def y_index():
    return render_template('login.html')
#app.add_
#路由到登录页面
@app.route('/login')
def user_login():
    return render_template('login.html')
@app.route('/aa')
def root():
    return app.root_path
#路由到注册页面
@app.route('/register')
def user_register():
    return render_template('register.html')
#@app.route('/register_user_db',methods=['POST'])
#def register_user_db():
#   pass

@app.route('/user_auth',methods=['POST'])
def user_auth():
    userName = request.form['username']
    passwd = request.form['pwd']
    u_info = user.query.filter_by(username=userName).first()
    if u_info:
        if u_info.password == passwd:
            return redirect('userorder.html')
        else:
            return "用户密码错误，请重新输入"
    else:
        return "用户认证失败，请检查用户名和密码是否正确"


if __name__ == "__main__":
    app.run(host=HOST,port=listen_port,debug=True,threaded=True)



