from fabric.api import *

env.user = 'root'
env.hosts = ['192.168.0.22', '192.168.0.67']
env.roledefs = {
    'webserver': ['192.168.0.189', '192.168.0.59'],
    'dbserver': ['192.168.0.67']
}
env.passwords = {
    'root@192.168.0.189:22': '123123123',
    'root@192.168.0.59:22': '123123',
    'root@192.168.0.67:22': '123123'
}

def input_raw():
    return prompt("please input directory name:",default='/home')

def worktask(dirname):
    run('ls -l '+dirname)

@roles('dbserver')
def go():
    getdirname = input_raw()
    worktask(getdirname)
