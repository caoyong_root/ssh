'''
#!usr/bin/env python3
# ssh 权限自动化开通系统

__author__ = 'caoyong'

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api

class APP(Flask):
    def __init__(self, config):
        Flask.__init__(self, __name__)
        self.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://%s:%s@%s:%s/%s' %(config.mariadb["user"],
                                                                            config.mariadb["password"],
                                                                            config.mariadb["host"],
                                                                            config.mariadb["port"],
                                                                            config.mariadb["database"]
                                                                            )
        self.config['SQLALCHEMY_ECHO'] = False
        self.config['SQLALCHEMY_POOL_SIZE'] = int(config.mariadb["db-connection-pool"])
        self.config['DEBUG'] = False
        self.db = SQLAlchemy(self, session_options={'autocommit': True})

    def get_db(self):
        # 连接数据库
        return self.db

    def get_api(self):
        return Api(self,catch_all_404s=True)

'''
