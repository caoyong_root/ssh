from fabric.api import *
from fabric.contrib.files import append

with open('/tmp/ip.txt') as f1:
    ip = f1.read()
with open('/tmp/id_rsa.pub') as f2:
    pub_key = f2.read()

env.user = 'root'
env.hosts = [ip]
env.passwd = '123123'

def worktask():
    run('mkdir -p /root/.ssh')
    append('/root/.ssh/authorized_keys',pub_key)

@task()
def go():
    execute(worktask)
