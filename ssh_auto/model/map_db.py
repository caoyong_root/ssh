'''
from runserver import app
from flask import request
from flask_restful import Resource
import time

db = app.get_db()
api = app.get_api()

class user(Resource):
    __tablename__ = 'user'
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String)
    passwd = db.Column(db.String)
    department = db.Column(db.String)
    manager = db.Column(db.String)
    date = db.Column(db.Date)

class userApi(Resource):
    def post(self):
        user_info = user(username=request.form['username'],passwd=request.form['passwd'],department=request.form['dept'],manager=request.form['lader'],date=time.strftime('%Y%m%d'))
        print(user_info)
        db.session.add(user_info)
        db.session.flush()
        db.session.commit()
        return "注册成功"

    def get(self):
        pass
    def delete(self):
        pass
    def put(self):
        pass
    def options(self):
        pass

api.add_resource(userApi,'/v1/api/register_user_db')


'''
