#!/usr/bin/python
#coding=utf-8

import time
from selenium import webdriver
import sys
from scrapy.http import HtmlResponse
import settings

class WebkitDownloaderTest( object ):
    def process_request( self, request, spider ):
        driver = webdriver.PhantomJS(executable_path='/windows/phantomjs/bin/phantomjs')
        driver.get(request.url)
        renderedBody = driver.page_source.encode('utf-8')
        return HtmlResponse(request.url,body=renderedBody)
