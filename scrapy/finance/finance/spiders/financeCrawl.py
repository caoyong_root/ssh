# -*- coding: utf-8 -*-
import scrapy
from scrapy.selector import Selector
from finance.items import *

class FinancecrawlSpider(scrapy.Spider):
    name = 'financeCrawl'
    allowed_domains = ['sina.com.cn']
    start_urls = ['http://finance.sina.com.cn/stock/sl/#concept_1']
    
    #def start_requests(self):
        
    def parse(self, response):
        body = response.body
        sel = Selector(text=body)
        tbodys = sel.xpath('//div[@class="tblOuter"]/table/tbody/tr')
        finance_names = []
        for tbody in tbodys:
            zding = FinanceItem()
            zding['plate_name'] = tbody.xpath('td[1]/a/text()').extract()[0]
            zding['com_num'] = tbody.xpath('td[2]/text()').extract()[0]
            finance_names.append(zding)
        return finance_names
