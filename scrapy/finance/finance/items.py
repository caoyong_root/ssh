# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FinanceItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    plate_name = scrapy.Field()
    #plate_url = scrapy.Filed()
    com_num = scrapy.Field()
    #avg_price = scrapy.Filed()
    #price_range = scrapy.Filed()
    #price_fluct = scrapy.Filed()
